using System;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using FluentValidationDemo.Validation;
using FluentValidation.AspNetCore;
using FluentValidationDemo.Localization.Localizer;

namespace FluentValidationDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddFluentValidation(c => c.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddSingleton<IStringLocalizerFactory, InMemoryStringLocalizerFactory>();
            services.AddTransient<IStringLocalizer, InMemoryStringLocalizer>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ConfigureFluentValidation(app.ApplicationServices);

            app.UseRequestLocalization((o) => {
                var cultures = new string[] {
                    "pt-BR",
                    "en-US"
                };
                o.AddSupportedUICultures(cultures);
                o.AddSupportedCultures(cultures);
                o.SetDefaultCulture(cultures[0]);
            });

            if (env.IsDevelopment())
            {   
                app.UseDeveloperExceptionPage();
            }

            // app.UseHttpsRedirection();

            app.UseRouting();

            // app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private void ConfigureFluentValidation(IServiceProvider serviceProvider)
        {
            var stringLocalizer = serviceProvider.GetRequiredService<IStringLocalizer>();
            ValidatorOptions.Global.DisplayNameResolver = (type, member, expression) => {
                if (member != null) {
                    return stringLocalizer.GetString(member.Name);
                }
                return null;
            };

            ValidatorOptions.Global.LanguageManager = new AppLanguageManager();
        }
    }
}
