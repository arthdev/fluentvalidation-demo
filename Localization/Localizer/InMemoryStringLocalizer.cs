using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FluentValidationDemo.Localization.Resources;
using Microsoft.Extensions.Localization;

namespace FluentValidationDemo.Localization.Localizer
{
    public class InMemoryStringLocalizer : IStringLocalizer
    {
        public LocalizedString this[string name] => new LocalizedString(name, Localize(name));

        public LocalizedString this[string name, params object[] arguments] => 
            new LocalizedString(name, Localize(name, arguments));

        public IEnumerable<LocalizedString> GetAllStrings(bool includeParentCultures)
        {
            return GetAll().Select(x => new LocalizedString(x.Key, x.Value));
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }

        private string Localize(string name)
        {
            return StringsManager.Get(name, CultureInfo.CurrentCulture);
        }

        private string Localize(string name, params object[] arguments)
        {
            string value = Localize(name);

            object[] localizedArgs = new string[arguments.Length];
            for (int i = 0; i < arguments.Length; i++)
            {
                localizedArgs[i] = Localize(arguments[i].ToString());       
            }

            return string.Format(value, localizedArgs);
        }

        private IReadOnlyDictionary<string, string> GetAll()
        {
            return StringsManager.GetAll(CultureInfo.CurrentCulture);
        }
    }
}