using System;
using Microsoft.Extensions.Localization;

namespace FluentValidationDemo.Localization.Localizer
{
    public class InMemoryStringLocalizerFactory : IStringLocalizerFactory
    {
        public IStringLocalizer Create(string baseName, string location)
        {
            return new InMemoryStringLocalizer();
        }

        public IStringLocalizer Create(Type resourceSource)
        {
            return new InMemoryStringLocalizer();
        }
    }
}