using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FluentValidationDemo.Localization.Resources
{
    public static class PtBrStrings
    {
        public const string Culture = "pt-BR";
        private static readonly IReadOnlyDictionary<string, string> _strings = 
            new ReadOnlyDictionary<string, string>(
                new Dictionary<string, string>() 
                {
                    { "Description", "Descrição" },
                    { "StartDate", "Data de Início" },
                    { "EndDate", "Data de Término" },
                    { "'{PropertyName}' must be fewer than '{ComparasionProp}'", "'{PropertyName}' deve ser menor que '{ComparasionProp}'"}
                }
            );

        public static IReadOnlyDictionary<string, string> Strings 
        { 
            get 
            {
                return _strings;
            }
        }
    }
}