using System.Collections.Generic;
using System.Collections.Immutable;
using System.Globalization;

namespace FluentValidationDemo.Localization.Resources
{
    public static class StringsManager
    {
        public static string Get(string name, CultureInfo culture)
        {
            string value;
            GetAll(culture).TryGetValue(name, out value);
            return value;
        }

        public static IReadOnlyDictionary<string, string> GetAll(CultureInfo culture)
        {
            switch (culture.Name)
            {
                case PtBrStrings.Culture:
                    return PtBrStrings.Strings;
                default:
                    return ImmutableDictionary<string, string>.Empty;
            }
        }
    }
}