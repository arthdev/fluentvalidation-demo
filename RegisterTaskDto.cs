namespace FluentValidationDemo
{
    public class RegisterTaskDto
    {
       public string Description { get; set; }
       public string StartDate { get; set; }
       public string EndDate { get; set; } 
    }
}