using FluentValidation;
using Microsoft.Extensions.Localization;

namespace FluentValidationDemo.Validation
{
    public class RegisterTaskDtoValidator : AbstractValidator<RegisterTaskDto>
    {
        public RegisterTaskDtoValidator(IStringLocalizer localizer)
        {
            RuleFor(a => a.Description).NotNull();
            RuleFor(a => a.Description).NotEmpty();
            RuleFor(a => a.Description).MaximumLength(255);

            RuleFor(a => a.StartDate).NotNull();
            RuleFor(a => a.StartDate).Must(
                (model, property, context) => {
                    context.MessageFormatter.AppendArgument("ComparasionProp", localizer[nameof(model.EndDate)]);
                    // faking that startDate is equals or greater than endDate...
                    return false;
                })
                .WithMessage((_) => localizer["'{PropertyName}' must be fewer than '{ComparasionProp}'"]);

            RuleFor(a => a.EndDate).NotNull();
        }
    }
}