using System;
using FluentValidation.Validators;

namespace FluentValidationDemo.Validation
{
    public class AppLanguageManager : FluentValidation.Resources.LanguageManager 
    {
        public AppLanguageManager() {
            OverrideDefaultMessages();
            AddCustomValidatorsTranslations();
        }

        private void AddCustomValidatorsTranslations()
        {
            // AddTranslation("pt-BR", nameof(DateTimeValidator), "'{PropertyName}' é inválido.");
        }

        private void OverrideDefaultMessages()
        {
            AddTranslation("pt-BR", nameof(NotNullValidator<object, object>), "'{PropertyName}' é obrigatório.");
            AddTranslation("pt-BR", nameof(NotEmptyValidator<object, object>), "'{PropertyName}' é obrigatório.");
        }
    }
}